-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: photogallery
-- ------------------------------------------------------
-- Server version	5.7.17-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `photo_album`
--

DROP TABLE IF EXISTS `photo_album`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photo_album` (
  `album_id` int(11) NOT NULL AUTO_INCREMENT,
  `album_name` varchar(90) NOT NULL,
  `album_description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`album_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photo_album`
--

LOCK TABLES `photo_album` WRITE;
/*!40000 ALTER TABLE `photo_album` DISABLE KEYS */;
INSERT INTO `photo_album` VALUES (1,'Nature','Animals, weather conditions, landscape.'),(2,'Family','All kind of family pictures.'),(3,'Weddings','Oh happy day!'),(4,'People','Interesting people from small places around the world.'),(9,'Birthdays','Birthdays and anniversaries.'),(10,'Sport','Major sport events and athletes at their best.'),(11,'Pets','Mostly cats, dogs and sheep.'),(12,'Helsinki','Beautiful scenery from Finland'),(13,'Tallinn','Scenery from Estonia'),(14,'Riga','Scenery from Riga'),(15,'Bird and Tree','Lovely magpie and oaktree'),(16,'Money','Greetings from Las Vegas'),(17,'Ain','Ain eating buscuits'),(18,'Siiri','Siiri eating candies'),(19,'Rain','Rain eating raisins'),(20,'Vesta','Vesta drinking tea'),(21,'Mari','Mari eating peanuts'),(28,'Test','Test123'),(30,'Hello World','Gello world 222'),(31,'Album','New album'),(32,'Album2','New testalbum'),(34,'Album 3','Testtest'),(35,'Testalbum','One more'),(36,'Animals','Lovely animals from around the world'),(37,'Test1','Testing');
/*!40000 ALTER TABLE `photo_album` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-20 16:23:02
