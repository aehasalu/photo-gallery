package ee.bcs.photogallery.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.photogallery.bean.Photo;

public class PhotoService {

	public PhotoService() {
	}

	public List<Photo> getAllPhotos(int albumId) {
		List<Photo> listOfPhotos = new ArrayList<>();
		Connection connection = DBConnection.createConnection();

		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"SELECT * FROM photogallery.photo_view WHERE album_id = '" + albumId + "';");) {
			while (resultSet.next()) {
				Photo photo = new Photo();
				photo.setAlbumId(resultSet.getInt("photo_album_id"));
				photo.setPhotoDescription(resultSet.getString("photo_description"));
				photo.setPhotoId(resultSet.getInt("photo_id"));
				photo.setPhoto(resultSet.getString("photo"));
				photo.setPlace(resultSet.getString("place"));
				photo.setDate(resultSet.getString("date"));
				photo.setTime(resultSet.getString("time"));

				listOfPhotos.add(photo);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnection.closeConnection(connection);
		return listOfPhotos;
	}
	
	public Photo addPhoto(Photo photo) {
		Connection connection = DBConnection.createConnection();
		try (Statement statement = connection.createStatement();) {
				statement.executeUpdate("INSERT INTO photogallery.pictures"
						+ "(photo_album_id, photo, photo_description, date, time, place) VALUES"
						+ "('" + photo.getAlbumId() + "', '" + photo.getPhoto() + "', '" + photo.getPhotoDescription() + "','" + photo.getDate() + "',"
						+ " '" + photo.getTime() + "', '" + photo.getPlace() + "');"); 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnection.closeConnection(connection);
		return photo;
	}
	
	public void deletePhoto(int id) {
		Connection connection = DBConnection.createConnection();
		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate("DELETE FROM pictures WHERE photo_id= '" + id + "';");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnection.closeConnection(connection);
	}
	
	public void deleteAllPhotos(int id) {
		Connection connection = DBConnection.createConnection();
		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate("DELETE FROM pictures WHERE photo_album_id= '" + id + "';");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnection.closeConnection(connection);
	}
}

