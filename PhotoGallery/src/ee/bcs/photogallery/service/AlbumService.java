package ee.bcs.photogallery.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ee.bcs.photogallery.bean.Album;

public class AlbumService {
	public AlbumService() {}
	
	public List<Album> getAllAlbums() {
		List<Album> listOfAlbums = new ArrayList<>();
		Connection connection = DBConnection.createConnection();
		try (Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * FROM photogallery.photo_album;");) {
			while (resultSet.next()) {
				Album album = new Album();
				album.setAlbumName(resultSet.getString("album_name"));
				album.setAlbumDescription(resultSet.getString("album_description"));
				album.setAlbumId(resultSet.getInt("album_id"));
				listOfAlbums.add(album);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnection.closeConnection(connection);
		return listOfAlbums;
	}
	
	public Album addAlbum(Album album) {
		Connection connection = DBConnection.createConnection();
		try (Statement statement = connection.createStatement();) {
				statement.executeUpdate("INSERT INTO photogallery.photo_album"
						+ "(album_name, album_description) VALUES"
						+ "('" + album.getAlbumName() + "', '" + album.getAlbumDescription() + "');"); 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnection.closeConnection(connection);
		return album;
	}
	
	public void deleteAlbum(int id) {
		Connection connection = DBConnection.createConnection();
		try (Statement statement = connection.createStatement();) {
			statement.executeUpdate("DELETE FROM photo_album WHERE album_id= '" + id + "';");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnection.closeConnection(connection);
	}

	public Album updateAlbum(Album album) {
		Connection connection = DBConnection.createConnection();
		try (Statement statement = connection.createStatement();) {
				statement.executeUpdate("UPDATE photogallery.photo_album SET "
						+ "album_name = '" + album.getAlbumName() + "',"
						+ "album_description = '" + album.getAlbumDescription() + "'"
						+ "WHERE album_id = '" + album.getAlbumId() + "';"); 
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBConnection.closeConnection(connection);
		return album;
	}
}