package ee.bcs.photogallery.controller;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.photogallery.bean.Photo;
import ee.bcs.photogallery.service.PhotoService;

@Path("/pictures")
public class PhotoController {
	
	PhotoService photoservice = new PhotoService();
	
	@GET
	@Path("/{albumId}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Photo> getPhotos(@PathParam("albumId") int albumId) {
		List<Photo> listOfPhotos = photoservice.getAllPhotos(albumId);
		return listOfPhotos;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Photo addPhoto(Photo photo) {
		return photoservice.addPhoto(photo);
	}
	
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public void deletePhoto(@PathParam("id") int id) {
		photoservice.deletePhoto(id);
	}	
	@DELETE
	@Path("/deleteall/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteAllPhotos(@PathParam("id") int id) {
		photoservice.deleteAllPhotos(id);
	}	
}

