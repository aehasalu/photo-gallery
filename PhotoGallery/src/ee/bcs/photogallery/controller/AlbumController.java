package ee.bcs.photogallery.controller;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ee.bcs.photogallery.bean.Album;
import ee.bcs.photogallery.service.AlbumService;

@Path("/albums")
public class AlbumController {
	
	AlbumService albumservice = new AlbumService();
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Album> getAlbums() {
		List<Album> listOfAlbums = albumservice.getAllAlbums();
		return listOfAlbums;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Album addAlbum(Album album) {
		return albumservice.addAlbum(album);
	}
	
	@DELETE
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public void deleteAlbum(@PathParam("id") int id) {
		albumservice.deleteAlbum(id);
	}
		
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Album updateAlbum(Album album) {
		return albumservice.updateAlbum(album);
	}
}

