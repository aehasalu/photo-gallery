$(function() {
	$('#albums-link').click(updateAlbums);
	$('#content').on('click', '.panel', openAlbum);
	$('#content').on('click', '.delete', deleteAlbum);
	$('#content').on('click', '.edit-button', editAlbum);
	$('#content').on('click', '.delete-photo', deletePhoto);
	$('#contacts-link').click(showContacts);
	$('.delete-all-photos').click(deleteAllPhotos);
	
	$('#add-album').submit(function(e) {
		e.preventDefault();
		var newAlbumJson = $(this).serializeFormJSON();
		var newAlbumData = JSON.stringify(newAlbumJson);
		$.ajax({
			url : 'rest/albums',
			cache : false,
			type : 'POST',
			data : newAlbumData,
			success : function(newAlbumData) {
				document.getElementById("add-album").reset();
				alert('Album added');
				updateAlbums();
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				console.log('Status: ' + textStatus);
				console.log('Error: ' + errorThrown);
			},
			contentType : 'application/json; charset=UTF-8'
		});
	});
	
	$('#add-photo').submit(function(e) {
		e.preventDefault();
		var newPhotoJson = $(this).serializeFormJSON();
		newPhotoJson.photo = imageData;
		var newPhotoData = JSON.stringify(newPhotoJson);
		
		$.ajax({
			url : 'rest/pictures',
			cache : false,
			type : 'POST',
			data : newPhotoData,
			success : function(newPhotoData) {
				document.getElementById('add-photo').reset();
				updatePhotos();
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				console.log('Status: ' + textStatus);
				console.log('Error: ' + errorThrown);
			},
			contentType : 'application/json; charset=UTF-8'
		});
	});
	
	$('#content').on('click', '#img', function() {
		$('.modal-title').append(this.parentElement.getElementsByClassName("photo-title")[0].innerHTML);
		$('.modal-content').attr('src', this.src);
		$('#myModal').modal();
	});
	
	$('#myModal').on('hidden.bs.modal', function() {
	    $(this).find('h4').html('');
	});
	
});

var imageData;
$(function() {
	$('#choosePhoto').on('change', function() {
		var fr = new FileReader();
		fr.onload = function(e) {
			imageData = this.result;
		};
		var src = this;
		fr.readAsDataURL(src.files[0]);
	});
});

var showContacts = function(e) {
	e.preventDefault();
	$('#content').empty();
	$('#add-album').hide();
	$('#add-photo').hide();
	$('.contacts').show();
	
	setTimeout(function() {
		var tallinn = {
				lat : 59.4270683,
				lng : 24.7465446
		};
		var map = new google.maps.Map(document.getElementById('map'), {
			zoom : 17,
			center : tallinn
		});
		var marker = new google.maps.Marker({
			position : tallinn,
			map : map
		});
	}, 0);
};

var updateAlbums = function(e) {
	if (e) e.preventDefault();
	
	$('#add-album').show();
	$('#add-photo').hide();
	$('.contacts').hide();
	$.get('rest/albums', function(albums) {
		var content = $('#content');
		content.empty();
		for (var i = 0; i < albums.length; i++) {
			var albumData = albums[i];
			var albumElement = $('<a href="#" id="album" class="panel panel-default" name="'
					+ albumData.albumName + '"/>');
			albumElement.data('album-id', albumData.albumId);
			var deleteElement = $('<a href="#" class="delete"><span class="glyphicon glyphicon-remove-circle" style="color: red;"></span></a>');
			var editElement = $('<button type="button" class="edit-button btn btn-primary">Edit</button>');
			var albumTitleElement = $('<h4>');
			var albumDescriptionElement = $('<p>');
			albumTitleElement.text(albumData.albumName);
			albumDescriptionElement.text(albumData.albumDescription);
			albumElement.append(albumTitleElement, albumDescriptionElement, deleteElement, editElement);
			content.append(albumElement);
		}
	});
};

var albumId;
var openAlbum = function(e) {
	if (e) e.preventDefault();
	
	$('#add-album').hide();
	$('.contacts').hide();
	$('#add-photo').show();
	albumId = $(this).data('album-id');
	$('#hiddenId').val(albumId);
	updatePhotos();
}

var updatePhotos = function(e) {
	if (e) e.preventDefault();
	
	$.get('rest/pictures/' + albumId, function(photos) {
		var content = $('#content');
		content.empty();
		var photoElement = $('<div class="row">');
		for (var i = 0; i < photos.length; i++) {
			var photoData = photos[i];
			var photoGrid = $('<div class="col-sm-6 col-md-4 col-lg-3">');
			var thumbnail = $('<div class="thumbnail">');
			thumbnail.data('photo-id', photoData.photoId);
			var photoDisplay = $('<img id="img" src=""/>');
			var photoContent = photoData.photo;
			photoDisplay.attr('src', photoContent);
			var photoTitleElement = $('<div class="caption">');
			var photoTitle = $('<p class="photo-title">');
			var title = photoData.photoDescription + ' ' + photoData.place;
			photoTitle.text(title);
			var photoTime = $('<p>');
			var time = photoData.date + ' ' + photoData.time;
			photoTime.text(time);
			var deleteElement = $('<a href="#" class="delete-photo"><span class="glyphicon glyphicon-remove-circle" style="color: red;"></span></a>');
			photoTitleElement.append(photoTitle, photoTime);
			thumbnail.append(photoDisplay, photoTitleElement, deleteElement);
			photoGrid.append(thumbnail);
			photoElement.append(photoGrid);
			content.append(photoElement);
		}
	});
};

var editAlbum = function(e) {
	e.stopPropagation();
	$('#edit-album-modal').modal();
};

var deleteAlbum = function(e) {
	e.stopPropagation();
	var element = $(this);
	var albumId = element.parent('#album').data('album-id');
	var r = confirm('Are you sure you want to delete this album?');
	if (r == true) {
		$.ajax({
			url : 'rest/albums/' + albumId,
			cache : false,
			type : 'DELETE',
			success : function() {
				updateAlbums();
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				console.log('Status: ' + textStatus);
				console.log('Error: ' + errorThrown);
			},
			contentType : 'application/json; charset=UTF-8'
		});
	} 
};

var deletePhoto = function(e) {
	e.stopPropagation();
	var element = $(this);
	var photoId = element.parent('.thumbnail').data('photo-id');
	var r = confirm('Are you sure you want to delete this photo?');
	if (r == true) {
		$.ajax({
			url : 'rest/pictures/' + photoId,
			cache : false,
			type : 'DELETE',
			success : function() {
				updatePhotos();
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				console.log('Status: ' + textStatus);
				console.log('Error: ' + errorThrown);
			},
			contentType : 'application/json; charset=UTF-8'
		});
	}
};

var deleteAllPhotos = function(e) {
	e.preventDefault();
	var r = confirm('Are you sure you want to delete all photos?');
	if (r == true) {
		$.ajax({
			url : 'rest/pictures/deleteall/' + albumId,
			cache : false,
			type : 'DELETE',
			success : function() {
				updatePhotos();
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				console.log('Status: ' + textStatus);
				console.log('Error: ' + errorThrown);
			},
			contentType : 'application/json; charset=UTF-8'
		});
	}
};

(function($) {
	$.fn.serializeFormJSON = function() {
		var o = {};
		var a = this.serializeArray();
		$.each(a, function() {
			if (o[this.name]) {
				if (!o[this.name].push) {
					o[this.name] = [ o[this.name] ];
				}
				o[this.name].push(this.value || '');
			} else {
				o[this.name] = this.value || '';
			}
		});
		return o;
	};
})(jQuery);
